/*****************************************************************************
*
* Copyright (c) 2016, Deok-Hwa Kim
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. Neither the name of the KAIST nor the
*    names of its contributors may be used to endorse or promote products
*    derived from this software without specific prior written permission.
* 4. It does not guarantee any patent licenses.
*
* THIS SOFTWARE IS PROVIDED BY DEOK-HWA KIM ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL DEOK-HWA KIM BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
********************************************************************************/

#ifndef __FRAME_HPP__
#define __FRAME_HPP__

#include <parameter.hpp>

#include <Eigen/Eigen>
#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <tuple>
#include <map>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#include <boost/lexical_cast.hpp>

//#include <opencv2/features2d/features2d.hpp>
//#include <opencv2/nonfree/nonfree.hpp>
//#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/core/eigen.hpp>
#include <tbb/tbb.h>


namespace goodguy {
class frame {
public:
    frame(const frame& target)
        : m_bgm(target.m_bgm), m_labeled_bgm(target.m_labeled_bgm),
          m_key(target.m_key),
          m_intensity(target.m_intensity), m_depth(target.m_depth),
          m_x_derivative(target.m_x_derivative), m_y_derivative(target.m_y_derivative),
          m_global_pose(target.m_global_pose),
          m_param(target.m_param)
    {

    }

    frame(
        const std::size_t key,
        const cv::Mat& intensity,
        const cv::Mat& depth,
        camera_parameter& param)
        : m_key(key),
          m_global_pose(NULL),
          m_param(param)
    {
        m_bgm = NULL;
        m_labeled_bgm = NULL;
        m_intensity = std::make_shared<Eigen::MatrixXf>(Eigen::MatrixXf(goodguy::frame::cv2eigen(intensity)));
        m_depth = std::make_shared<Eigen::MatrixXf>(Eigen::MatrixXf(goodguy::frame::cv2eigen(depth)));
        m_x_derivative = calculate_derivative_x(m_intensity);
        m_y_derivative = calculate_derivative_y(m_intensity);
    }

    frame(
        const std::size_t key,
        std::shared_ptr<Eigen::MatrixXf>& intensity,
        std::shared_ptr<Eigen::MatrixXf>& depth,
        camera_parameter& param)
        : m_key(key),
          m_intensity(intensity),
          m_depth(depth),
          m_global_pose(NULL),
          m_param(param)
    {
        m_x_derivative = calculate_derivative_x(intensity);
        m_y_derivative = calculate_derivative_y(intensity);

        // This constructor doesn't need to generate ORB feature
        //   This constructor is only called when the frame is being half or double
    }

    frame()
        : m_global_pose(NULL)
    { }


    bool empty() {
        return m_depth == NULL;
    }



    void set(std::shared_ptr<Eigen::MatrixXf>& intensity,
             std::shared_ptr<Eigen::MatrixXf>& depth,
             std::shared_ptr<Eigen::MatrixXf>& x_derivative,
             std::shared_ptr<Eigen::MatrixXf>& y_derivative,
             camera_parameter& param)
    {
        m_intensity = intensity;
        m_depth = depth;
        m_x_derivative = x_derivative;
        m_y_derivative = y_derivative;
        m_param = param;
    }

    void set_intensity(std::shared_ptr<Eigen::MatrixXf>& intensity) {
        m_intensity = intensity;
    }
    void set_depth(std::shared_ptr<Eigen::MatrixXf>& depth) {
        m_depth = depth;
    }
    void set_x_derivative(std::shared_ptr<Eigen::MatrixXf>& x_derivative) {
        m_x_derivative = x_derivative;
    }
    void set_y_derivative(std::shared_ptr<Eigen::MatrixXf>& y_derivative) {
        m_y_derivative = y_derivative;
    }
    void set_param(camera_parameter& param) {
        m_param = param;
    }

    void set_global_pose(const Eigen::Matrix4f& pose) {
        m_global_pose = std::make_shared<Eigen::Matrix4f>(Eigen::Matrix4f(pose));
    }

    std::shared_ptr<Eigen::Matrix4f> get_global_pose() {
        return m_global_pose;
    }

    void set_key(std::size_t key) {
        m_key = key;
    }

    int cols() const {
        return m_intensity->cols();
    }
    int rows() const {
        return m_intensity->rows();
    }

    std::size_t get_key() const {
        return m_key;
    }

    std::shared_ptr<goodguy::frame> get_half_frame() {
        camera_parameter half_param = m_param;
        half_param.fx /= 2.0;
        half_param.fy /= 2.0;
        half_param.cx /= 2.0;
        half_param.cy /= 2.0;
        auto half_intensity = get_half_image(m_intensity);
        auto half_depth = get_half_image(m_depth);

        return std::make_shared<goodguy::frame>(goodguy::frame(m_key, half_intensity, half_depth, half_param));
    }

    std::shared_ptr<goodguy::frame> get_double_frame() {
        camera_parameter double_param = m_param;
        double_param.fx *= 2.0;
        double_param.fy *= 2.0;
        double_param.cx *= 2.0;
        double_param.cy *= 2.0;

        auto double_intensity = get_double_image(m_intensity);
        auto double_depth = get_double_image(m_depth);

        return std::make_shared<goodguy::frame>(goodguy::frame(m_key, double_intensity, double_depth,  double_param));
    }

    std::shared_ptr<Eigen::MatrixXf>& get_intensity() {
        return m_intensity;
    }
    std::shared_ptr<Eigen::MatrixXf>& get_depth() {
        return m_depth;
    }
    std::shared_ptr<Eigen::MatrixXf>& get_x_derivative() {
        return m_x_derivative;
    }
    std::shared_ptr<Eigen::MatrixXf>& get_y_derivative() {
        return m_y_derivative;
    }
    camera_parameter& get_param() {
        return m_param;
    }


    static std::shared_ptr<Eigen::MatrixXf> get_double_image(const std::shared_ptr<Eigen::MatrixXf>& image) {
        std::shared_ptr<Eigen::MatrixXf> double_image(new Eigen::MatrixXf(Eigen::MatrixXf::Zero(image->rows()*2, image->cols()*2)));
        auto lambda_for_double = [&](const tbb::blocked_range<int>& r) {
            for(int j = r.begin(); j < r.end(); j += 1) {
                for(int i = 0; i < image->rows(); i += 1) {
                    int i_plus = std::min(i+1, (int)image->rows()-1);
                    int j_plus = std::min(j+1, (int)image->cols()-1);
                    (*double_image)(2*i+0,2*j+0) = (*image)(i+0,j+0);
                    (*double_image)(2*i+0,2*j+1) = (*image)(i+0,j+0)*0.5 + (*image)(i+0,j_plus)*0.5;
                    (*double_image)(2*i+1,2*j+0) = (*image)(i+0,j+0)*0.5 + (*image)(i_plus,j+0)*0.5;
                    (*double_image)(2*i+1,2*j+1) = (*image)(i+0,j+0)*0.5 + (*image)(i_plus,j_plus)*0.5;
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0,image->cols()), lambda_for_double);
        return double_image;
    }

    static std::shared_ptr<Eigen::MatrixXf> get_half_image(const std::shared_ptr<Eigen::MatrixXf>& image) {
        std::shared_ptr<Eigen::MatrixXf> half_image(new Eigen::MatrixXf(Eigen::MatrixXf::Zero(image->rows()/2, image->cols()/2)));
        auto lambda_for_half = [&](const tbb::blocked_range<int>& r) {
            for(int j = r.begin(); j < r.end(); j += 2) {
                for(int i = 0; i < image->rows(); i += 2) {
                    (*half_image)(i/2,j/2) = (*image)(i,j);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0,image->cols()), lambda_for_half);
        return half_image;
    }

    static Eigen::MatrixXf cv2eigen(const cv::Mat& depth) {
        Eigen::MatrixXf depth_eigen;
        cv::cv2eigen(depth, depth_eigen);
        return depth_eigen;
    }

    static cv::Mat eigen2cv(const Eigen::MatrixXf& depth) {
        cv::Mat depth_cv;
        cv::eigen2cv(depth,depth_cv);
        return depth_cv;
    }



private:


    std::shared_ptr<Eigen::MatrixXf> calculate_derivative_x(const std::shared_ptr<Eigen::MatrixXf>& intensity) {
        std::shared_ptr<Eigen::MatrixXf> derivative_x(new Eigen::MatrixXf(Eigen::MatrixXf::Zero(intensity->rows(), intensity->cols())));

        auto lambda_for_derivative = [&](const tbb::blocked_range<int>& r) {
            for(int j = r.begin(); j < r.end(); ++j) {
                for(int i = 0; i < intensity->rows()/4; ++i) {

                    int left = std::max(j-1, 0);
                    int right = std::min(j+1, (int)intensity->cols()-1);

                    int up = std::max(i*4-1, 0);
                    int down = std::min(i*4+4, (int)intensity->rows()-1);

                    __m128 ul, ur;
                    if((i*4-1) < 0) {
                        ul = _mm_set_ps((*intensity)(up+2, left), (*intensity)(up+2, left), (*intensity)(up+1, left), (*intensity)(up+0, left));
                        ur = _mm_set_ps((*intensity)(up+2, right), (*intensity)(up+2, right), (*intensity)(up+1, right), (*intensity)(up+0, right));
                    }
                    else {
                        ul = _mm_set_ps((*intensity)(up+3, left), (*intensity)(up+2, left), (*intensity)(up+1, left), (*intensity)(up+0, left));
                        ur = _mm_set_ps((*intensity)(up+3, right), (*intensity)(up+2, right), (*intensity)(up+1, right), (*intensity)(up+0, right));
                    }


                    __m128 cl = _mm_load_ps(intensity->data()+left*intensity->rows()+i*4);
                    __m128 cr = _mm_load_ps(intensity->data()+right*intensity->rows()+i*4);


                    __m128 dl, dr;
                    if(i*4+4 > ((int)intensity->rows()-1)) {
                        dl = _mm_set_ps((*intensity)(down-0, left), (*intensity)(down-0, left), (*intensity)(down-1, left), (*intensity)(down-2, left));
                        dr = _mm_set_ps((*intensity)(down-0, right), (*intensity)(down-0, right), (*intensity)(down-1, right), (*intensity)(down-2, right));
                    }
                    else {
                        dl = _mm_set_ps((*intensity)(down-0, left), (*intensity)(down-1, left), (*intensity)(down-2, left), (*intensity)(down-3, left));
                        dr = _mm_set_ps((*intensity)(down-0, right), (*intensity)(down-1, right), (*intensity)(down-2, right), (*intensity)(down-3, right));
                    }

                    __m128 val =  _mm_add_ps(_mm_add_ps(_mm_add_ps(ur, dr), cr), cr);
                    __m128 val2 = _mm_sub_ps(_mm_sub_ps(_mm_sub_ps(_mm_sub_ps(val, ul), dl), cl), cl);
                    __m128 val3 = _mm_mul_ps(val2, _mm_set1_ps(0.125));

                    _mm_store_ps(derivative_x->data()+j*(int)intensity->rows()+4*i, val3);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0,intensity->cols()), lambda_for_derivative);

        return derivative_x;
    }

    std::shared_ptr<Eigen::MatrixXf> calculate_derivative_y(const std::shared_ptr<Eigen::MatrixXf>& intensity) {
        std::shared_ptr<Eigen::MatrixXf> derivative_y(new Eigen::MatrixXf(Eigen::MatrixXf::Zero(intensity->rows(), intensity->cols())));

        auto lambda_for_derivative = [&](const tbb::blocked_range<int>& r) {
            for(int j = r.begin(); j < r.end(); ++j) {
                for(int i = 0; i < intensity->rows()/4; ++i) {

                    int left = std::max(j-1, 0);
                    int right = std::min(j+1, (int)intensity->cols()-1);

                    int up = std::max(i*4-1, 0);
                    int down = std::min(i*4+4, (int)intensity->rows()-1);

                    __m128 ul, uc, ur;
                    if((i*4-1) < 0) {
                        ul = _mm_set_ps((*intensity)(up+2, left), (*intensity)(up+1, left), (*intensity)(up+0, left), (*intensity)(up+0, left));
                        uc = _mm_set_ps((*intensity)(up+2, j), (*intensity)(up+1, j), (*intensity)(up+0, j), (*intensity)(up+0, j));
                        ur = _mm_set_ps((*intensity)(up+2, right), (*intensity)(up+1, right), (*intensity)(up+0, right), (*intensity)(up+0, right));
                    }
                    else {
                        ul = _mm_set_ps((*intensity)(up+3, left), (*intensity)(up+2, left), (*intensity)(up+1, left), (*intensity)(up+0, left));
                        uc = _mm_set_ps((*intensity)(up+3, j), (*intensity)(up+2, j), (*intensity)(up+1, j), (*intensity)(up+0, j));
                        ur = _mm_set_ps((*intensity)(up+3, right), (*intensity)(up+2, right), (*intensity)(up+1, right), (*intensity)(up+0, right));
                    }

                    __m128 dl, dc, dr;
                    if(i*4+4 > ((int)intensity->rows()-1)) {
                        dl = _mm_set_ps((*intensity)(down-0, left), (*intensity)(down-0, left), (*intensity)(down-1, left), (*intensity)(down-2, left));
                        dc = _mm_set_ps((*intensity)(down-0, j), (*intensity)(down-0, j), (*intensity)(down-1, j), (*intensity)(down-2, j));
                        dr = _mm_set_ps((*intensity)(down-0, right), (*intensity)(down-0, right), (*intensity)(down-1, right), (*intensity)(down-2, right));
                    }
                    else {
                        dl = _mm_set_ps((*intensity)(down-0, left), (*intensity)(down-1, left), (*intensity)(down-2, left), (*intensity)(down-3, left));
                        dc = _mm_set_ps((*intensity)(down-0, j), (*intensity)(down-1, j), (*intensity)(down-2, j), (*intensity)(down-3, j));
                        dr = _mm_set_ps((*intensity)(down-0, right), (*intensity)(down-1, right), (*intensity)(down-2, right), (*intensity)(down-3, right));
                    }

                    __m128 val =  _mm_add_ps(_mm_add_ps(_mm_add_ps(dr, dl), dc), dc);
                    __m128 val2 = _mm_sub_ps(_mm_sub_ps(_mm_sub_ps(_mm_sub_ps(val, ul), ur), uc), uc);
                    __m128 val3 = _mm_mul_ps(val2, _mm_set1_ps(0.125));

                    _mm_store_ps(derivative_y->data()+j*(int)intensity->rows()+4*i, val3);
                }
            }
        };
        tbb::parallel_for(tbb::blocked_range<int>(0,intensity->cols()), lambda_for_derivative);

        return derivative_y;

    }

public:

    std::shared_ptr<Eigen::MatrixXf> m_bgm;
    std::shared_ptr<Eigen::MatrixXf> m_labeled_bgm;



public:

    std::mutex m_mutex;

    std::size_t m_key;

    std::shared_ptr<Eigen::MatrixXf> m_intensity;
    std::shared_ptr<Eigen::MatrixXf> m_depth;
    std::shared_ptr<Eigen::MatrixXf> m_x_derivative;
    std::shared_ptr<Eigen::MatrixXf> m_y_derivative;

    std::shared_ptr<Eigen::Matrix4f> m_global_pose;

    camera_parameter m_param;
};
}

#endif
