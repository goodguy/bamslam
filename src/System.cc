/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/



#include "System.h"
#include "Converter.h"
#include <thread>
#include <pangolin/pangolin.h>
#include <iomanip>

#include "Optimizer.h"

namespace ORB_SLAM2
{

System::System(const string &strVocFile, const string &strSettingsFile, const eSensor sensor,
               const bool bUseViewer):mSensor(sensor), mpViewer(static_cast<Viewer*>(NULL)), mbReset(false),mbActivateLocalizationMode(false),
        mbDeactivateLocalizationMode(false)
{
    // Output welcome message
    cout << endl <<
    "ORB-SLAM2 Copyright (C) 2014-2016 Raul Mur-Artal, University of Zaragoza." << endl <<
    "This program comes with ABSOLUTELY NO WARRANTY;" << endl  <<
    "This is free software, and you are welcome to redistribute it" << endl <<
    "under certain conditions. See LICENSE.txt." << endl << endl;

    cout << "Input sensor was set to: ";

    cout << "RGB-D" << endl;

    //Check settings file
    cv::FileStorage fsSettings(strSettingsFile.c_str(), cv::FileStorage::READ);
    if(!fsSettings.isOpened())
    {
       cerr << "Failed to open settings file at: " << strSettingsFile << endl;
       exit(-1);
    }


    //Load ORB Vocabulary
    cout << endl << "Loading ORB Vocabulary. This could take a while..." << endl;

    mpVocabulary = new ORBVocabulary();
    bool bVocLoad = mpVocabulary->loadFromTextFile(strVocFile);
    if(!bVocLoad)
    {
        cerr << "Wrong path to vocabulary. " << endl;
        cerr << "Falied to open at: " << strVocFile << endl;
        exit(-1);
    }
    cout << "Vocabulary loaded!" << endl << endl;

    //Create KeyFrame Database
    mpKeyFrameDatabase = new KeyFrameDatabase(*mpVocabulary);

    //Create the Map
    mpMap = new Map();

    //Create Drawers. These are used by the Viewer
    mpFrameDrawer = new FrameDrawer(mpMap);
    mpMapDrawer = new MapDrawer(mpMap, strSettingsFile);

    //Initialize the Tracking thread
    //(it will live in the main thread of execution, the one that called this constructor)
    mpTracker = new Tracking(this, mpVocabulary, mpFrameDrawer, mpMapDrawer,
                             mpMap, mpKeyFrameDatabase, strSettingsFile, mSensor);

    //Initialize the Local Mapping thread and launch
    mpLocalMapper = new LocalMapping(mpMap, false);
    mptLocalMapping = new thread(&ORB_SLAM2::LocalMapping::Run,mpLocalMapper);

    //Initialize the Loop Closing thread and launch
    mpLoopCloser = new LoopClosing(mpMap, mpKeyFrameDatabase, mpVocabulary, true);
    mptLoopClosing = new thread(&ORB_SLAM2::LoopClosing::Run, mpLoopCloser);

    //Initialize the Viewer thread and launch
    if(bUseViewer)
    {
        mpViewer = new Viewer(this, mpFrameDrawer,mpMapDrawer,mpTracker,strSettingsFile);
        mptViewer = new thread(&Viewer::Run, mpViewer);
        mpTracker->SetViewer(mpViewer);
    }

    //Set pointers between threads
    mpTracker->SetLocalMapper(mpLocalMapper);
    mpTracker->SetLoopClosing(mpLoopCloser);

    mpLocalMapper->SetTracker(mpTracker);
    mpLocalMapper->SetLoopCloser(mpLoopCloser);

    mpLoopCloser->SetTracker(mpTracker);
    mpLoopCloser->SetLocalMapper(mpLocalMapper);
}

std::pair<Eigen::Affine3d, std::size_t> System::get_current_pose() {
    unique_lock<mutex> lock(mMutexMode);
    Eigen::Matrix4d pose_mat = Eigen::Matrix4d::Identity();
    cv::cv2eigen(mpTracker->mCurrentFrame.mTcw, pose_mat);
    std::size_t kf_index = mpTracker->mCurrentFrame.mpReferenceKF->mnId;
    return std::make_pair(Eigen::Affine3d(pose_mat), kf_index);
}

bool System::query_pose(const cv::Mat& color, const cv::Mat& depth, const std::size_t kf_index, double timestamp, Eigen::Affine3d& computed_pose) {

    unique_lock<mutex> lock(mMutexMode);

    cv::Mat imGray = color;
    cv::Mat imDepth = depth;

    if(imGray.channels()==3)
    {
        cvtColor(imGray,imGray,CV_BGR2GRAY);
    }
    else if(imGray.channels()==4)
    {
        cvtColor(imGray,imGray,CV_BGRA2GRAY);
    }

    //imDepth.convertTo(imDepth,CV_32F, mpTracker->mDepthMapFactor);
    //std::cout << "Hello" << std::endl;


    cv::Mat curr_intensity;
    imGray.convertTo(curr_intensity, CV_32FC1, 1.0/255.0);


    if(imGray.empty() || imDepth.empty()) {
        std::cout << "Received Empty images" << std::endl;
        return false;
    }

    //std::cout << "Depth: " << imDepth.at<float>(120,180) << std::endl;



    Frame mCurrentFrame = Frame(imGray,imDepth,timestamp,mpTracker->mpORBextractorQuery,mpTracker->mpORBVocabulary,mpTracker->mK,mpTracker->mDistCoef,mpTracker->mbf,mpTracker->mThDepth);


    // Compute Bag of Words Vector
    mCurrentFrame.ComputeBoW();
    std::vector<KeyFrame*> keyframes = mpMap->GetAllKeyFrames();

    KeyFrame* near_kf = NULL;

    for(std::size_t i = 0; i < keyframes.size(); ++i){
        if(keyframes[i]->isBad()){
            continue;
        }

        if(near_kf != NULL) {
            if(std::abs((long)near_kf->mnId - (long)kf_index) >= std::abs((long)keyframes[i]->mnId - (long)kf_index)) {
                near_kf = keyframes[i];
            }
        }
        else {
            near_kf = keyframes[i];
        }
    }

    if(near_kf == NULL) {
        return false;
    }

    // We perform first an ORB matching with each candidate
    // If enough matches are found we setup a PnP solver
    ORBmatcher matcher(0.75,true);

    PnPsolver* vpPnPsolvers;
    std::vector<MapPoint*> vvpMapPointMatches;

    std::cout << "near_kf: " << near_kf->mnId << " / " << kf_index << std::endl;

    bool vbDiscarded = false;
    if(near_kf->isBad())
        vbDiscarded = true;
    else
    {
        /*
        int nmatches = matcher.SearchByBoW(near_kf,mCurrentFrame,vvpMapPointMatches);
        if(nmatches<15)
        {   
            vbDiscarded = true;
        }
        else
        {
            PnPsolver* pSolver = new PnPsolver(mCurrentFrame,vvpMapPointMatches);
            pSolver->SetRansacParameters(0.99,10,300,4,0.5,5.991);
            vpPnPsolvers = pSolver;
        }
        */

		set<MapPoint*> sFound;
		near_kf->GetPose().copyTo(mCurrentFrame.mTcw);
        int nmatches =matcher.SearchByProjection(mCurrentFrame,near_kf,sFound,10,100);
        std::cout << "Matches: " << nmatches << std::endl;

        for(int ip =0; ip<mCurrentFrame.N; ip++)
            if(mCurrentFrame.mvpMapPoints[ip])
                vvpMapPointMatches.push_back(mCurrentFrame.mvpMapPoints[ip]);

        if(nmatches<15)
        {   
            vbDiscarded = true;
        }
        else
        {
            PnPsolver* pSolver = new PnPsolver(mCurrentFrame,vvpMapPointMatches);
            pSolver->SetRansacParameters(0.99,10,300,4,0.5,5.991);
            vpPnPsolvers = pSolver;
        }
    }

    bool bMatch = false;
    ORBmatcher matcher2(0.9,true);

    if(vbDiscarded) {
        std::cout << "Does not match at first matching" << std::endl;
        return false;
    }

    // Perform 5 Ransac Iterations
    vector<bool> vbInliers;
    int nInliers;
    bool bNoMore;

    PnPsolver* pSolver = vpPnPsolvers;
    cv::Mat Tcw = pSolver->iterate(5,bNoMore,vbInliers,nInliers);

    // If Ransac reachs max. iterations discard keyframe
    if(bNoMore)
    {
        vbDiscarded=true;
    }

	// If a Camera Pose is computed, optimize
	if(!Tcw.empty())
	{
		Tcw.copyTo(mCurrentFrame.mTcw);

		set<MapPoint*> sFound;

		const int np = vbInliers.size();

		for(int j=0; j<np; j++)
		{
			if(vbInliers[j])
			{
				mCurrentFrame.mvpMapPoints[j]=vvpMapPointMatches[j];
				sFound.insert(vvpMapPointMatches[j]);
			}
			else
				mCurrentFrame.mvpMapPoints[j]=NULL;
		}

		int nGood = Optimizer::PoseOptimization(&mCurrentFrame);

		if(nGood<10) {
            std::cout << "Does not match at second matching" << std::endl;
			return false;
		}

		for(int io =0; io<mCurrentFrame.N; io++)
			if(mCurrentFrame.mvbOutlier[io])
				mCurrentFrame.mvpMapPoints[io]=static_cast<MapPoint*>(NULL);

		// If few inliers, search by projection in a coarse window and optimize again
		if(nGood<50)
		{
			int nadditional =matcher2.SearchByProjection(mCurrentFrame,near_kf,sFound,10,100);

			if(nadditional+nGood>=50)
			{
				nGood = Optimizer::PoseOptimization(&mCurrentFrame);

				// If many inliers but still not enough, search by projection again in a narrower window
				// the camera has been already optimized with many points
				if(nGood>30 && nGood<50)
				{
					sFound.clear();
					for(int ip =0; ip<mCurrentFrame.N; ip++)
						if(mCurrentFrame.mvpMapPoints[ip])
							sFound.insert(mCurrentFrame.mvpMapPoints[ip]);
					nadditional =matcher2.SearchByProjection(mCurrentFrame,near_kf,sFound,3,64);

					// Final optimization
					if(nGood+nadditional>=50)
					{
						nGood = Optimizer::PoseOptimization(&mCurrentFrame);

						for(int io =0; io<mCurrentFrame.N; io++)
							if(mCurrentFrame.mvbOutlier[io])
								mCurrentFrame.mvpMapPoints[io]=NULL;
					}
				}
			}
		}


		// If the pose is supported by enough inliers stop ransacs and continue
		if(nGood>=50)
		{
			bMatch = true;
		}
	}

    //std::cout << "Successfully found pose" << std::endl;
    Eigen::Matrix4d pose_mat = Eigen::Matrix4d::Identity();
    cv::cv2eigen(mCurrentFrame.mTcw, pose_mat);
    computed_pose = Eigen::Affine3d(pose_mat);

    //std::cout << computed_pose.matrix() << std::endl;

    return true;
}


#ifdef BAMSLAM
cv::Mat System::TrackRGBD(const cv::Mat &im, const cv::Mat &depthmap, const cv::Mat& bgm, const Eigen::Affine3d& pose,  const double &timestamp)
#else
cv::Mat System::TrackRGBD(const cv::Mat &im, const cv::Mat &depthmap, const double &timestamp)
#endif
{
    if(mSensor!=RGBD)
    {
        cerr << "ERROR: you called TrackRGBD but input sensor was not set to RGBD." << endl;
        exit(-1);
    }    

    // Check mode change
    {
        unique_lock<mutex> lock(mMutexMode);
        if(mbActivateLocalizationMode)
        {
            mpLocalMapper->RequestStop();

            // Wait until Local Mapping has effectively stopped
            while(!mpLocalMapper->isStopped())
            {
                usleep(1000);
            }

            mpTracker->InformOnlyTracking(true);
            mbActivateLocalizationMode = false;
        }
        if(mbDeactivateLocalizationMode)
        {
            mpTracker->InformOnlyTracking(false);
            mpLocalMapper->Release();
            mbDeactivateLocalizationMode = false;
        }
    }

    // Check reset
    {
    unique_lock<mutex> lock(mMutexReset);
    if(mbReset)
    {
        mpTracker->Reset();
        mbReset = false;
    }
    }

#ifdef BAMSLAM
    cv::Mat Tcw = mpTracker->GrabImageRGBD(im,depthmap,bgm,pose,timestamp);
#else
    cv::Mat Tcw = mpTracker->GrabImageRGBD(im,depthmap,timestamp);
#endif

    unique_lock<mutex> lock2(mMutexState);
    mTrackingState = mpTracker->mState;
    mTrackedMapPoints = mpTracker->mCurrentFrame.mvpMapPoints;
    mTrackedKeyPointsUn = mpTracker->mCurrentFrame.mvKeysUn;
    std::cout << "TRACKING FINISH!" << std::endl;

    //SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

    return Tcw;
}


void System::ActivateLocalizationMode()
{
    unique_lock<mutex> lock(mMutexMode);
    mbActivateLocalizationMode = true;
}

void System::DeactivateLocalizationMode()
{
    unique_lock<mutex> lock(mMutexMode);
    mbDeactivateLocalizationMode = true;
}

bool System::MapChanged()
{
    static int n=0;
    int curn = mpMap->GetLastBigChangeIdx();
    if(n<curn)
    {
        n=curn;
        return true;
    }
    else
        return false;
}

void System::Reset()
{
    unique_lock<mutex> lock(mMutexReset);
    mbReset = true;
}

void System::Shutdown()
{
    mpLocalMapper->RequestFinish();
    mpLoopCloser->RequestFinish();
    if(mpViewer)
    {
        mpViewer->RequestFinish();
        while(!mpViewer->isFinished())
            usleep(5000);
    }

    // Wait until all thread have effectively stopped
    while(!mpLocalMapper->isFinished() || !mpLoopCloser->isFinished() || mpLoopCloser->isRunningGBA())
    {
        usleep(5000);
    }

    if(mpViewer)
        pangolin::BindToContext("ORB-SLAM2: Map Viewer");
}

void System::SaveTrajectoryTUM(const string &filename)
{
    cout << endl << "Saving camera trajectory to " << filename << " ..." << endl;

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
    // We need to get first the keyframe pose and then concatenate the relative transformation.
    // Frames not localized (tracking failure) are not saved.

    // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
    // which is true when tracking failed (lbL).
    list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
    list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
    list<bool>::iterator lbL = mpTracker->mlbLost.begin();
    for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(),
        lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++, lbL++)
    {
        if(*lbL)
            continue;

        KeyFrame* pKF = *lRit;

        cv::Mat Trw = cv::Mat::eye(4,4,CV_32F);

        // If the reference keyframe was culled, traverse the spanning tree to get a suitable keyframe.
        while(pKF->isBad())
        {
            Trw = Trw*pKF->mTcp;
            pKF = pKF->GetParent();
        }

        Trw = Trw*pKF->GetPose()*Two;

        cv::Mat Tcw = (*lit)*Trw;
        cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
        cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

        vector<float> q = Converter::toQuaternion(Rwc);

        f << setprecision(6) << *lT << " " <<  setprecision(9) << twc.at<float>(0) << " " << twc.at<float>(1) << " " << twc.at<float>(2) << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;
    }
    f.close();
    cout << endl << "trajectory saved!" << endl;
}


void System::SaveKeyFrameTrajectoryTUM(const string &filename)
{
    cout << endl << "Saving keyframe trajectory to " << filename << " ..." << endl;

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    //cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    for(size_t i=0; i<vpKFs.size(); i++)
    {
        KeyFrame* pKF = vpKFs[i];

       // pKF->SetPose(pKF->GetPose()*Two);

        if(pKF->isBad())
            continue;

        cv::Mat R = pKF->GetRotation().t();
        vector<float> q = Converter::toQuaternion(R);
        cv::Mat t = pKF->GetCameraCenter();
        f << setprecision(6) << pKF->mTimeStamp << setprecision(7) << " " << t.at<float>(0) << " " << t.at<float>(1) << " " << t.at<float>(2)
          << " " << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << endl;

    }

    f.close();
    cout << endl << "trajectory saved!" << endl;
}

void System::SaveTrajectoryKITTI(const string &filename)
{
    cout << endl << "Saving camera trajectory to " << filename << " ..." << endl;

    vector<KeyFrame*> vpKFs = mpMap->GetAllKeyFrames();
    sort(vpKFs.begin(),vpKFs.end(),KeyFrame::lId);

    // Transform all keyframes so that the first keyframe is at the origin.
    // After a loop closure the first keyframe might not be at the origin.
    cv::Mat Two = vpKFs[0]->GetPoseInverse();

    ofstream f;
    f.open(filename.c_str());
    f << fixed;

    // Frame pose is stored relative to its reference keyframe (which is optimized by BA and pose graph).
    // We need to get first the keyframe pose and then concatenate the relative transformation.
    // Frames not localized (tracking failure) are not saved.

    // For each frame we have a reference keyframe (lRit), the timestamp (lT) and a flag
    // which is true when tracking failed (lbL).
    list<ORB_SLAM2::KeyFrame*>::iterator lRit = mpTracker->mlpReferences.begin();
    list<double>::iterator lT = mpTracker->mlFrameTimes.begin();
    for(list<cv::Mat>::iterator lit=mpTracker->mlRelativeFramePoses.begin(), lend=mpTracker->mlRelativeFramePoses.end();lit!=lend;lit++, lRit++, lT++)
    {
        ORB_SLAM2::KeyFrame* pKF = *lRit;

        cv::Mat Trw = cv::Mat::eye(4,4,CV_32F);

        while(pKF->isBad())
        {
          //  cout << "bad parent" << endl;
            Trw = Trw*pKF->mTcp;
            pKF = pKF->GetParent();
        }

        Trw = Trw*pKF->GetPose()*Two;

        cv::Mat Tcw = (*lit)*Trw;
        cv::Mat Rwc = Tcw.rowRange(0,3).colRange(0,3).t();
        cv::Mat twc = -Rwc*Tcw.rowRange(0,3).col(3);

        f << setprecision(9) << Rwc.at<float>(0,0) << " " << Rwc.at<float>(0,1)  << " " << Rwc.at<float>(0,2) << " "  << twc.at<float>(0) << " " <<
             Rwc.at<float>(1,0) << " " << Rwc.at<float>(1,1)  << " " << Rwc.at<float>(1,2) << " "  << twc.at<float>(1) << " " <<
             Rwc.at<float>(2,0) << " " << Rwc.at<float>(2,1)  << " " << Rwc.at<float>(2,2) << " "  << twc.at<float>(2) << endl;
    }
    f.close();
    cout << endl << "trajectory saved!" << endl;
}

int System::GetTrackingState()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackingState;
}

vector<MapPoint*> System::GetTrackedMapPoints()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackedMapPoints;
}

vector<cv::KeyPoint> System::GetTrackedKeyPointsUn()
{
    unique_lock<mutex> lock(mMutexState);
    return mTrackedKeyPointsUn;
}

} //namespace ORB_SLAM
