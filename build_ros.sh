echo "Building ROS nodes"

cd Examples/ROS/bamslam
mkdir build
cd build
cmake .. -DROS_BUILD_TYPE=Release
make -j
