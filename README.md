# BaM-SLAM
**Authors:** [Deok-Hwa Kim](http://rit.kaist.ac.kr/home/dhkim) and [Jong-Hwan Kim](http://rit.kaist.ac.kr/home/jhkim/Biography_en)

BaM-SLAM is a real-time SLAM library for RGB-D camera that compute the camera trajectory, background model and  a sparse 3D feature map. Since it follows ORB-SLAM2 system, it is able to detect loops and relocalize the camera in real time. We provide comparative example to run the SLAM system in the [RIT dataset](), which contains dynamic motions in front of RGB-D camera. This system is work on ROS system to prcess live RGB-D stream.


###Related Publications:

[BaMVO] Deok-Hwa Kim and Jong-Hwan Kim. **Effective Background Model-Based RGB-D Dense Visual Odometry in a Dynamic Environment**. *IEEE Transactions on Robotics,* vol. 32, no. 6, pp. 1565-1573, 2016. **[PDF](http://rit.kaist.ac.kr/home/International_Journal?action=AttachFile&do=get&target=paper_0394.pdf)**.

[ORB-SLAM2] Raúl Mur-Artal and Juan D. Tardós. **ORB-SLAM2: an Open-Source SLAM System for Monocular, Stereo and RGB-D Cameras**. *IEEE Transactions on Robotics*  vol. 33, no. 5, pp. 1255-1262, 2017. **[PDF](https://128.84.21.199/pdf/1610.06475.pdf)**.


#1. Prerequisites
We have tested the library in **Ubuntu 16.04**, but it should be easy to compile in other platforms. A powerful computer (e.g. i7) will ensure real-time performance and provide more stable and accurate results.
Actually, we follow the pre-requisites of ORB-SLAM2 with that ROS installation is mandantory.

## C++11 or C++0x Compiler
We use the new thread and chrono functionalities of C++11.

## Pangolin
We use [Pangolin](https://github.com/stevenlovegrove/Pangolin) for visualization and user interface. Dowload and install instructions can be found at: https://github.com/stevenlovegrove/Pangolin.

## OpenCV
We use [OpenCV](http://opencv.org) to manipulate images and features. Dowload and install instructions can be found at: http://opencv.org. **Required at leat 2.4.3. Tested with OpenCV 2.4.11 and OpenCV 3.2**.

## Eigen3
Required by g2o (see below). Download and install instructions can be found at: http://eigen.tuxfamily.org. **Required at least 3.1.0**.

## DBoW2 and g2o (Included in Thirdparty folder)
We use modified versions of the [DBoW2](https://github.com/dorian3d/DBoW2) library to perform place recognition and [g2o](https://github.com/RainerKuemmerle/g2o) library to perform non-linear optimizations. Both modified libraries (which are BSD) are included in the *Thirdparty* folder.

## ROS 
BaM-SLAM is worked on a [ROS](ros.org) platform for sharing information with BaMVO and OpenNI node. We have tested in Kinetic verion of ROS.

#2. Building BaM-SLAM and BaMVO 

Clone the repositories:
```
cd ~
mkdir -p ~/research_ws/src  && cd ~/research_ws/src && catkin_init_workspace
git clone https://bitbucket.org/goodguy/bamvo    bamvo
git clone https://bitbucket.org/goodguy/bamslam  bamslam
```

We follow the script of ORB-SLAM2 to build *BaM-SLAM* and *Thirdparty* libraries by using `build.sh`. Please make sure you have installed all required dependencies (see section 1). Execute:
```
cd bamslam
chmod +x build.sh
./build.sh
./build_ros.sh
```
This will create **libORB_SLAM2.so** and **libBAM_SLAM.so** at *lib* folder.

We use *Catkin* build system to build *BaMVO* node. Execute:
```
cd ~/research_ws/
catkin_make -j3
source ~/research_ws/devel/setup.bash
```
This will create **BaMVO** package.


#3. Download RIT dataset

## RIT Dataset

1. Download a sequence from http://vision.in.tum.de/data/datasets/rgbd-dataset/download and uncompress it.

2. Execute the following command. Change `TUMX.yaml` to TUM1.yaml,TUM2.yaml or TUM3.yaml for freiburg1, freiburg2 and freiburg3 sequences respectively. Change `PATH_TO_SEQUENCE_FOLDER`to the uncompressed sequence folder.
```
./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt Examples/Monocular/TUMX.yaml PATH_TO_SEQUENCE_FOLDER
```
