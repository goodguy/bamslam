/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<thread>
#include<mutex>

#include<ros/ros.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h>
#include <eigen_conversions/eigen_msg.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>

#include <vslam_simple_navigation/GetPose.h>
#include <vslam_simple_navigation/Frame.h>

#include <geometry_msgs/PoseStamped.h>

#include<opencv2/core/core.hpp>

#include"../../../include/System.h"

using namespace std;

float scale = 1.0;

ros::Publisher g_pub_next;
ros::Publisher g_pub_slam;
ros::Publisher g_pub_frame;

tf::TransformBroadcaster* g_tf_broadcaster;
tf::TransformListener* g_tf_listener;

class ImageGrabber
{
public:
    ImageGrabber(ORB_SLAM2::System* pSLAM):m_pose(Eigen::Affine3d::Identity()), mpSLAM(pSLAM){}

#ifdef BAMSLAM
    void GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD,const sensor_msgs::ImageConstPtr& msg_bgm, const geometry_msgs::PoseStampedConstPtr& msg_pose);
#else
    void GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD);
#endif
    std::size_t get_kf_index()
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_pose);
        return m_kf_index;

    }
    Eigen::Affine3d get_pose()
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_pose);
        return m_pose;

    }
    void set_pose(const Eigen::Affine3d& pose, const std::size_t kf_index)
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_pose);
        m_pose = pose;
        m_kf_index = kf_index;
    }

    ros::Time get_received_time()
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_pose);
        return m_received_time;

    }
    void set_received_time(const ros::Time& received_time)
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_pose);
        m_received_time = received_time;
    }

    void set_images(const cv::Mat& color, const cv::Mat& depth)
    {
        std::lock_guard<std::mutex> lock(m_mutex_for_image);
        m_curr_color.reset(new cv::Mat(color));
        m_curr_depth.reset(new cv::Mat(depth));
    }

    std::pair<std::shared_ptr<cv::Mat>, std::shared_ptr<cv::Mat>> get_images( ) {
        std::lock_guard<std::mutex> lock(m_mutex_for_image);
        return std::make_pair(m_curr_color, m_curr_depth);
    }

    std::mutex m_mutex_for_pose;
    std::mutex m_mutex_for_image;


    Eigen::Affine3d m_pose;
    std::size_t m_kf_index;
    ros::Time m_received_time;

    std::shared_ptr<cv::Mat> m_curr_color;
    std::shared_ptr<cv::Mat> m_curr_depth;

    ORB_SLAM2::System* mpSLAM;
};



void run(ImageGrabber* grabber){
    Eigen::Affine3d initial_pose = Eigen::Affine3d::Identity();
    bool is_initialized = false;

    while(ros::ok()){
        tf::StampedTransform base2rgb_tf;
        tf::StampedTransform base2odom_tf;
        std::string base_frame_name = "base_footprint";
        std::string rgb_frame_name = "xtion_rgb_optical_frame";
        std::string slam_frame_name = "map";
        std::string odom_frame_name = "odom";
        try {
            g_tf_listener->lookupTransform(base_frame_name, rgb_frame_name, ros::Time(0), base2rgb_tf);
            g_tf_listener->lookupTransform(base_frame_name, odom_frame_name, ros::Time(0), base2odom_tf);
        } catch (tf::TransformException& ex) {
            //ROS_ERROR("TRANSFORM EXCEPTION: %s", ex.what());
            continue;
        }

        Eigen::Affine3d base2rgb_eigen;
        tf::transformTFToEigen(base2rgb_tf, base2rgb_eigen);

        Eigen::Affine3d base2odom_eigen;
        tf::transformTFToEigen(base2odom_tf, base2odom_eigen);

        if(!is_initialized) {
            is_initialized = true;
            initial_pose = base2rgb_eigen;
        }

        Eigen::Affine3d global_pose_eigen = grabber->get_pose();
        ros::Time received_time = grabber->get_received_time();
        Eigen::Affine3d diff_robot_state = base2rgb_eigen * initial_pose.inverse() ;

        global_pose_eigen = base2rgb_eigen * global_pose_eigen * base2rgb_eigen.inverse() * diff_robot_state;



        geometry_msgs::PoseStamped local_camera_pose;
        tf::poseEigenToMsg(global_pose_eigen, local_camera_pose.pose);
        local_camera_pose.header.stamp = received_time;
        local_camera_pose.header.frame_id = rgb_frame_name;

        geometry_msgs::PoseStamped global_pose;
        tf::poseEigenToMsg(global_pose_eigen, global_pose.pose);

        tf::Transform odom2map_tf;

        Eigen::Affine3d odom2map = base2odom_eigen.inverse() * global_pose_eigen;
        tf::transformEigenToTF(odom2map, odom2map_tf);
        g_tf_broadcaster->sendTransform(tf::StampedTransform(odom2map_tf, received_time, odom_frame_name,  slam_frame_name));

        vslam_simple_navigation::Frame frame_msg;
        frame_msg.kf_index.data = grabber->get_kf_index();
        frame_msg.pose = global_pose.pose;

        std::pair<std::shared_ptr<cv::Mat>, std::shared_ptr<cv::Mat>> curr_images = grabber->get_images();

        cv_bridge::CvImage cv_color;
        cv_bridge::CvImage cv_depth;

        cv_color.header = local_camera_pose.header;
        cv_depth.header = local_camera_pose.header;
        cv_color.encoding = "bgr8";
        cv_depth.encoding = "32FC1";

        if(curr_images.first == NULL)   continue;
        if(curr_images.second == NULL)   continue;

        curr_images.first->copyTo(cv_color.image);

        if(curr_images.second->type() == CV_32FC1){
            curr_images.second->copyTo(cv_depth.image);
        }
        else{
            curr_images.second->convertTo(cv_depth.image, CV_32FC1, 1.0/1000.0);
        }


        frame_msg.color_image = *(cv_color.toImageMsg());
        frame_msg.depth_image = *(cv_depth.toImageMsg());

        g_pub_frame.publish(frame_msg);



        nav_msgs::Odometry msg;
        msg.header.stamp = received_time;
        msg.header.frame_id = slam_frame_name;
        msg.child_frame_id = base_frame_name;
        msg.pose.pose = global_pose.pose;
        g_pub_slam.publish(msg);
        



        ros::Duration(0.1).sleep();

    }
}

bool query_pose_service(
        vslam_simple_navigation::GetPose::Request& req, 
        vslam_simple_navigation::GetPose::Response& res, 
        ImageGrabber* grabber
        )
{
    cv_bridge::CvImageConstPtr cv_ptrRGB;
    try
    {
        cv_ptrRGB = cv_bridge::toCvCopy(req.frame.color_image);
    }
    catch (cv_bridge::Exception& e)
    {
        //ROS_ERROR("cv_bridge exception: %s", e.what());
        return false;
    }

    cv_bridge::CvImageConstPtr cv_ptrD;
    try
    {
        cv_ptrD = cv_bridge::toCvCopy(req.frame.depth_image);
    }
    catch (cv_bridge::Exception& e)
    {
        //ROS_ERROR("cv_bridge exception: %s", e.what());
        return false;
    }

    cv::Mat color = cv_ptrRGB->image;
    cv::Mat depth = cv_ptrD->image;

    std::size_t kf_index = req.frame.kf_index.data;
    double timestamp = cv_ptrRGB->header.stamp.toSec();

    Eigen::Affine3d pose = Eigen::Affine3d::Identity();
    if(!grabber->mpSLAM->query_pose(color, depth, kf_index, timestamp, pose)){
        std::cout << "Querying is failed " << std::endl;
        res.result = false;
        return false;
    }

    tf::StampedTransform base2rgb_tf;
    std::string base_frame_name = "base_footprint";
    std::string rgb_frame_name = "xtion_rgb_optical_frame";
    std::string slam_frame_name = "map";
    try {
        g_tf_listener->lookupTransform(base_frame_name, rgb_frame_name, ros::Time(0), base2rgb_tf);
    } catch (tf::TransformException& ex) {
        //ROS_ERROR("TRANSFORM EXCEPTION: %s", ex.what());
        res.result = false;
        return false;
    }


    Eigen::Affine3d base2rgb_eigen;
    tf::transformTFToEigen(base2rgb_tf, base2rgb_eigen);

    pose = base2rgb_eigen * pose * base2rgb_eigen.inverse();


    std::cout << "----------------" << std::endl;
    std::cout << pose.matrix() << std::endl;




    tf::poseEigenToMsg(pose, res.pose);

    res.result = true;
    return true;




}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "RGBD");
    ros::AsyncSpinner spinner(3);
    spinner.start();

    if(argc != 3)
    {
        cerr << endl << "Usage: rosrun ORB_SLAM2 RGBD path_to_vocabulary path_to_settings" << endl;        
        ros::shutdown();
        return 1;
    }
        
    ros::NodeHandle nh_local("~");
    nh_local.getParam("scale", scale);

    std::string save_loc;
    nh_local.getParam("save_loc", save_loc);

    std::string camera_name;
    nh_local.getParam("camera", camera_name);

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::RGBD,true);

    ImageGrabber igb(&SLAM);

    ros::NodeHandle nh;
    //g_pub_next = nh.advertise<std_msgs::Empty>(camera_name+std::string("/next_frame"), 50);

    g_pub_slam = nh.advertise<nav_msgs::Odometry>("slam", 50);
    g_pub_frame = nh.advertise<vslam_simple_navigation::Frame>("frame", 50);

    tf::TransformBroadcaster tf_broadcaster;
    tf::TransformListener tf_listener;
    g_tf_broadcaster = &tf_broadcaster;
    g_tf_listener = &tf_listener;

    auto lambda_for_run = [&](){
        std::cout <<"Run start" << std::endl;
        run(&igb);
        std::cout <<"Run end" << std::endl;
    };

    std::thread thread_for_run(lambda_for_run);


    ros::ServiceServer service = nh.advertiseService
        <vslam_simple_navigation::GetPose::Request, vslam_simple_navigation::GetPose::Response>
        (
         "query_pose",
         boost::bind(&query_pose_service, _1, _2, &igb)
        );




    message_filters::Subscriber<sensor_msgs::Image> rgb_sub(nh, camera_name + std::string("/rgb/image_raw"), 1);
    message_filters::Subscriber<sensor_msgs::Image> depth_sub(nh, camera_name + std::string("/depth_registered/image_raw"), 1);
#ifdef BAMSLAM
    message_filters::Subscriber<sensor_msgs::Image> bgm_sub(nh, camera_name + std::string("/bamvo/image_raw"), 1);
    message_filters::Subscriber<geometry_msgs::PoseStamped> pose_sub(nh, camera_name + std::string("/bamvo/pose"), 1);
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image, sensor_msgs::Image, geometry_msgs::PoseStamped> sync_pol;
    message_filters::Synchronizer<sync_pol> sync(sync_pol(1000), rgb_sub,depth_sub, bgm_sub, pose_sub);
    sync.registerCallback(boost::bind(&ImageGrabber::GrabRGBD,&igb,_1,_2,_3,_4));
#else
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
    message_filters::Synchronizer<sync_pol> sync(sync_pol(1000), rgb_sub,depth_sub);
    sync.registerCallback(boost::bind(&ImageGrabber::GrabRGBD,&igb,_1,_2));
#endif

    ros::waitForShutdown();

    // Stop all threads
    SLAM.Shutdown();

    // Save camera trajectory
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

    ros::shutdown();

    return 0;
}

#ifdef BAMSLAM
void ImageGrabber::GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD,const sensor_msgs::ImageConstPtr& msg_bgm, const geometry_msgs::PoseStampedConstPtr& msg_pose)
#else
void ImageGrabber::GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD)
#endif
{
    std::cout << "GRAB IMAGES"<< std::endl;
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptrRGB;
    try
    {
        cv_ptrRGB = cv_bridge::toCvShare(msgRGB);
    }
    catch (cv_bridge::Exception& e)
    {
        //ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv_bridge::CvImageConstPtr cv_ptrD;
    try
    {
        cv_ptrD = cv_bridge::toCvShare(msgD);
    }
    catch (cv_bridge::Exception& e)
    {
        //ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

#ifdef BAMSLAM
    cv_bridge::CvImageConstPtr cv_ptr_bgm;
    try
    {
        cv_ptr_bgm = cv_bridge::toCvShare(msg_bgm);
    }
    catch (cv_bridge::Exception& e)
    {
        //ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

#endif

    cv::Mat resize_rgb, resize_depth;
#ifdef BAMSLAM
    cv::Mat resize_bgm;
    Eigen::Affine3d pose = Eigen::Affine3d::Identity();
    tf::poseMsgToEigen(msg_pose->pose, pose);
#endif

    cv::Size target_size(cv_ptrRGB->image.cols*scale, cv_ptrRGB->image.rows*scale);

    cv::resize(cv_ptrRGB->image, resize_rgb, target_size);
    cv::resize(cv_ptrD->image, resize_depth, target_size);
#ifdef BAMSLAM
    cv::resize(cv_ptr_bgm->image, resize_bgm, target_size);

#endif

#ifdef BAMSLAM
    mpSLAM->TrackRGBD(resize_rgb, resize_depth, resize_bgm, pose, cv_ptrRGB->header.stamp.toSec());
#else
    mpSLAM->TrackRGBD(resize_rgb, resize_depth, cv_ptrRGB->header.stamp.toSec());
#endif

    set_images(resize_rgb, resize_depth);

    std_msgs::Empty msg;

    g_pub_next.publish(msg);


    if(mpSLAM->GetTrackingState() == ORB_SLAM2::Tracking::OK){

        set_received_time(msgRGB->header.stamp);
        std::pair<Eigen::Affine3d, std::size_t> current_pose = mpSLAM->get_current_pose();
        set_pose(current_pose.first, current_pose.second);

    }

    std::cout << "END To TRACK"<< std::endl;


}


