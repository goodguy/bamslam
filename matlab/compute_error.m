

end_error_bamslam = zeros(1,6);
end_error_orbslam = zeros(1,6);

error_bamslam = zeros(1,6);
error_orbslam = zeros(1,6);

odom_trans_real = zeros(1, 3);
odom_trans_bamslam = zeros(1, 3);
odom_trans_orbslam = zeros(1, 3);

idx_valid = 1;

for idx = step+2:1:count-step-1
    
    next_pose_real = odom_real(:,:,idx+step);
    cur_pose_real = odom_real(:,:,idx);
    prev_pose_real = odom_real(:,:,idx-step);
    bet_pose_real = cur_pose_real \ prev_pose_real;
    %bet_pose_real2 = next_pose_real \ cur_pose_real;
    %bet_pose_real = prev_pose_real \ cur_pose_real;
    %bet_pose_real2 = cur_pose_real \ next_pose_real;
    
    
    
    cur_pose_bamslam = odom_bamslam(:,:,idx);
    prev_pose_bamslam = odom_bamslam(:,:,idx-step);
    bet_pose_bamslam = cur_pose_bamslam \ prev_pose_bamslam;
    %bet_pose_bamslam = prev_pose_bamslam \ cur_pose_bamslam;
    
    cur_pose_orbslam = odom_orbslam(:,:,idx);
    prev_pose_orbslam = odom_orbslam(:,:,idx-step);
    bet_pose_orbslam = cur_pose_orbslam \ prev_pose_orbslam;
    %bet_pose_orbslam = prev_pose_orbslam \ cur_pose_orbslam;
    
    pose_diff_bamslam = bet_pose_bamslam \ bet_pose_real;
    pose_diff_orbslam = bet_pose_orbslam \ bet_pose_real;
    
    
    
    %if(abs(mean(bet_pose_real2(1:3,4)))*2.0 < abs(mean(bet_pose_real(1:3,4))))
    temp = abs(pose_diff_bamslam(1:3,4));
    error_trans_temp = sqrt(temp(1).^2 + temp(2).^2 + temp(3).^2)/step;
    if(error_trans_temp > 0.02)
    %   continue; 
    end
    
    error_bamslam(idx_valid,1:3) = abs(pose_diff_bamslam(1:3,4));
    error_orbslam(idx_valid,1:3) = abs(pose_diff_orbslam(1:3,4));
        
    error_bamslam(idx_valid,4:6) = abs(matrix2rot(pose_diff_bamslam(1:3,1:3)));
    error_orbslam(idx_valid,4:6) = abs(matrix2rot(pose_diff_orbslam(1:3,1:3)));
    
    
    odom_trans_real(idx_valid,:) =  prev_pose_real(1:3,4)';
    odom_trans_bamslam(idx_valid,:) =  prev_pose_bamslam(1:3,4)';
    odom_trans_orbslam(idx_valid,:) =  prev_pose_orbslam(1:3,4)';
    
    idx_valid = idx_valid+1;
    
end


error_trans_bamslam = sqrt(error_bamslam(:,1).^2 + error_bamslam(:,2).^2 + error_bamslam(:,3).^2)/step;
error_rot_bamslam = sqrt(error_bamslam(:,4).^2 + error_bamslam(:,5).^2 + error_bamslam(:,6).^2)/step;
error_trans_orbslam = sqrt(error_orbslam(:,1).^2 + error_orbslam(:,2).^2 + error_orbslam(:,3).^2)/step;
error_rot_orbslam = sqrt(error_orbslam(:,4).^2 + error_orbslam(:,5).^2 + error_orbslam(:,6).^2)/step;

end_error_trans_bamslam = sqrt(end_error_bamslam(:,1).^2 + end_error_bamslam(:,2).^2 + end_error_bamslam(:,3).^2);
end_error_rot_bamslam = sqrt(end_error_bamslam(:,4).^2 + end_error_bamslam(:,5).^2 + end_error_bamslam(:,6).^2);
end_error_trans_orbslam = sqrt(end_error_orbslam(:,1).^2 + end_error_orbslam(:,2).^2 + end_error_orbslam(:,3).^2);
end_error_rot_orbslam = sqrt(end_error_orbslam(:,4).^2 + end_error_orbslam(:,5).^2 + end_error_orbslam(:,6).^2);

mean_error_bamslam = [mean(error_trans_bamslam) std(error_trans_bamslam) mean(error_rot_bamslam) std(error_rot_bamslam)]*1000 
mean_error_orbslam = [mean(error_trans_orbslam) std(error_trans_orbslam) mean(error_rot_orbslam) std(error_rot_orbslam)] *1000

end_error_bamslam_norm = [end_error_trans_bamslam end_error_rot_bamslam];
end_error_orbslam_norm = [end_error_trans_orbslam end_error_rot_orbslam];
