
clear all;
close all;

% 5, 9 (rpy) 영상에는 Auto-Exposure, Auto-Whitebalance가 꺼지지 않아 모션 추정이 잘되지 않음.
% 이러한 부분에서도 로버스트한 형태의 알고리즘이 필요하다 언급.


try_index = 3;

odom_real = zeros(4,4,1);
odom_bamslam= zeros(4,4,1);
odom_orbslam= zeros(4,4,1);


count = 0;
max = 4000;


odom_real_origin = eye(4,4);

odom_real_pose_convert = eye(4,4);
%odom_real_pose_convert(1:3,1:3) = rot_z(-90*pi/180) * rot_y(90*pi/180);



idx_real = 0;
for idx = 1:max
    target_real = strcat('/home/goodguy/database/navigation/tum/rgbd_dataset_freiburg2_desk/odom_', int2str(idx+1),'.txt');
    target_orbslam = strcat('/home/goodguy/result_orbslam/odom_', int2str(idx+1),'.txt');
    target_bamslam = strcat('/home/goodguy/result_bamslam/odom_', int2str(idx+1),'.txt');
    %target_bamslam = strcat('/home/goodguy/result_bamvo/odom_', int2str(idx+1),'.txt');
    
        
    if exist(target_real, 'file') ...
            & exist(target_orbslam, 'file') ...
            & exist(target_bamslam, 'file')  
            
       count = count+1;
       
       odom_real_curr = odom_real_pose_convert*load(target_real);
       odom_bamslam_curr = odom_real_pose_convert*load(target_bamslam);
       odom_orbslam_curr = odom_real_pose_convert*load(target_orbslam);


       
       if(count == 1)
           odom_real_origin = odom_real_curr;
       end
       
       %odom_real(:,:,count) = inv(odom_real_origin)*odom_real_curr;
       odom_real(:,:,count) = inv(odom_real_origin)*odom_real_curr;
       
       
       odom_orbslam(:,:,count) = inv(odom_orbslam_curr);
       odom_bamslam(:,:,count) = inv(odom_bamslam_curr);
       
       idx_real(count) = idx; 
       
    end
end

step = 50;
compute_error;



%odom_trans_real(:, 2) = -odom_trans_real(:, 2);
figure(1);

interval = 1;
odom_trans = odom_trans_real;
odom_trans_len = length(odom_trans);
plot3(odom_trans(1:odom_trans_len, 1), odom_trans(1:odom_trans_len, 2), odom_trans(1:odom_trans_len, 3), '.r','linewidth',2,'MarkerSize', 2);
axis equal;
hold on;
odom_trans = odom_trans_bamslam;
odom_trans_len = length(odom_trans);
plot3(odom_trans(1:interval:odom_trans_len, 1), odom_trans(1:interval:odom_trans_len, 2), odom_trans(1:interval:odom_trans_len, 3), '-.b','linewidth',2);
odom_trans = odom_trans_orbslam;
odom_trans_len = length(odom_trans);
plot3(odom_trans(1:interval:odom_trans_len, 1), odom_trans(1:interval:odom_trans_len, 2), odom_trans(1:interval:odom_trans_len, 3), ':g','linewidth',2);
hold off;
axis equal;

h_legend = legend('Ground truth', 'BaM-SLAM', 'ORB-SLAM', 'Location','northeast');
set(h_legend,'FontSize',12);
set(h_legend,'Interpreter', 'latex');

xlabel('x (m)', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('y (m)', 'Interpreter', 'latex', 'FontSize', 15)
zlabel('z (m)', 'Interpreter', 'latex', 'FontSize', 15)

set(gca, 'XTick', [-3:1:5]);
set(gca, 'YTick',[-4:1:2]);
set(gca, 'ZTick', [-1:0.5:3]);
set(gca, 'FontSize', 12);
set(gca, 'TickLabelInterpreter', 'latex');
set(gca, 'Box', 'off');



