clear all;
close all;
dataset = '/home/goodguy/database/bamslam/dynamic_hallway_15_3_16_1';
%dataset = '/home/goodguy/.ros';




for keyframe = 30:500
    frame = 0;
    
    depth_list = cell(1,100);
    bgm_list = cell(1,100);
    pose_list = cell(1,100);
    intensity_list = cell(1,100);
    
    count = 0;
    
    pre_pose = eye(4,4);
    
    for frame = 0:1000
        intensity_file_name = [dataset '/keyframe_' int2str(keyframe) '_intensity_' int2str(frame) '.txt'];
        depth_file_name = [dataset '/keyframe_' int2str(keyframe) '_depth_' int2str(frame) '.txt'];
        bgm_file_name = [dataset '/keyframe_' int2str(keyframe) '_labeled_bgm_' int2str(frame) '.txt'];
        pose_file_name = [dataset '/keyframe_' int2str(keyframe) '_pose_' int2str(frame) '.txt'];


        if ~exist(intensity_file_name, 'file') ...
            | ~exist(depth_file_name, 'file') ...
            | ~exist(bgm_file_name, 'file') ...
            | ~exist(pose_file_name, 'file')
            continue;
        end

        intensity = load(intensity_file_name);
        depth = load(depth_file_name);
        bgm = load(bgm_file_name);
        pose = load(pose_file_name);

        if isempty(pose)    continue;  end
        
        
        count = count + 1;
        
        if count == 1
           pre_pose = pose; 
           pre_depth = depth;
           figure(5);
           imshow(depth/5);
        end
        
        
        intensity_list{1, count} = intensity;
        depth_list{1, count} = depth;
        bgm_list{1, count} = bgm;
        pose_list{1, count} = pose;
        
        figure(1);
        imshow(intensity);
        figure(2);
        imshow(depth/5);
        figure(3);
        imshow(bgm/20);
        

    end
    
    if count == 0   continue; end
    
    warped_depth_list = cell(1, count);
    warped_bgm_list = cell(1, count);
    warped_intensity_list = cell(1, count);
    
    for k = 1:count
        
        between_pose = (pre_pose^-1 * pose_list{k})^-1;
        
        warped_depth_image = warp_depth_image(depth_list{k}, between_pose);
        warped_intensity = warp_image(intensity_list{k}, depth_list{k}, between_pose);
        warped_bgm = warp_image(bgm_list{k}, depth_list{k}, between_pose);
        
        warped_depth_list{k} = warped_depth_image;
        warped_intensity_list{k} = warped_intensity;
        warped_bgm_list{k} = warped_bgm;                
    end
    
    
    
    
    
    figure(6);
    imshow(warped_depth_image/5);
    figure(7);
    imshow(warped_intensity);        
    figure(8);
    imshow(warped_bgm_list{1}/10);
    
    
    
    

    
    

    
end

function warped_image = warp_depth_image(depth, pose)
    points = convert_3d_point_cloud(depth);
    transformed_points = transform_3d_point_cloud(points, pose);
    warped_image = project_depth_image(transformed_points,  size(depth, 2));
end

function warped_image = warp_image(image, depth, pose)
    points = convert_3d_point_cloud_with_image(depth, image);
    transformed_points = transform_3d_point_cloud(points, pose);
    warped_image = project_image(transformed_points,  size(depth, 2));
end



function [fx, fy, cx, cy] = get_cparam(cols)
    
    scale = 640/cols;
    
    fx = 549.094829 / scale;
    fy = 545.329258 / scale;
    cx = 292.782603 / scale;
    cy = 226.667207 / scale;
       
end

function points = convert_3d_point_cloud(depth)
    cols = size(depth,2);
    rows = size(depth,1);
    
    [fx, fy, cx, cy] = get_cparam(cols);
    
    min_depth = 0.5;
    max_depth = 7.0;
    
    points = zeros(3, cols * rows);
    
    for col = 0:cols-1
        for row = 0:rows-1
            depth_val = depth(row+1, col+1);
            if depth_val < min_depth | depth_val > max_depth
               depth_val = 0; 
            end
            
            x = (col-cx)*depth_val/ fx;
            y = (row-cy)*depth_val/ fy;
            z = depth_val;
            
            points(:, row*cols + col+1) = [x y z]';            
        end
    end
end



function points = convert_3d_point_cloud_with_image(depth, image)
    cols = size(depth,2);
    rows = size(depth,1);
    
    [fx, fy, cx, cy] = get_cparam(cols);
    
    min_depth = 0.5;
    max_depth = 7.0;
    
    points = zeros(4, cols * rows);
    
    for col = 0:cols-1
        for row = 0:rows-1
            depth_val = depth(row+1, col+1);
            image_val = image(row+1, col+1);

            if depth_val < min_depth | depth_val > max_depth
               depth_val = 0; 
            end
            
            x = (col-cx)*depth_val/ fx;
            y = (row-cy)*depth_val/ fy;
            z = depth_val;
            
            points(:, row*cols + col+1) = [x y z image_val]';            
        end
    end
end

function projected_image = project_depth_image(points, cols)
    rows = length(points)/ cols;
    
    projected_image = zeros(rows, cols);
    [fx, fy, cx, cy] = get_cparam(cols);

    for k = 1:length(points)
       %row = mod(k, cols);
       %col = (k-row)/cols;
       point = points(:,k);
       x = round(point(1)*fx/point(3) +cx);
       y = round(point(2)*fy/point(3) +cy);
       d = point(3);
       
       if isnan(x) | isnan(y)
           continue;
       end
       
       if x < 0 | x > cols-1 | y < 0 | y > rows-1 
           continue;
       end
       projected_image(y+1, x+1) = d;
    end

end

function projected_image = project_image(points, cols)
    rows = length(points)/ cols;
    
    projected_image = zeros(rows, cols);
    [fx, fy, cx, cy] = get_cparam(cols);

    for k = 1:length(points)
       %row = mod(k, cols);
       %col = (k-row)/cols;
       point = points(:,k);
       x = round(point(1)*fx/point(3) +cx);
       y = round(point(2)*fy/point(3) +cy);
       val = point(4);
       
       
       if isnan(x) | isnan(y)
           continue;
       end
       
       if x < 0 | x > cols-1 | y < 0 | y > rows-1 
           continue;
       end
       projected_image(y+1, x+1) = val;
    end

end

function transformed_points = transform_3d_point_cloud(points, pose)
    transformed_points = zeros(size(points));
    
    for k = 1:length(points)
        transformed_points(1:3, k) = pose(1:3,1:3)*points(1:3,k) + pose(1:3, 4);
        if size(points,1) == 4
            transformed_points(4, k) = points(4, k);
        end
    end
end

