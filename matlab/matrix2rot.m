function ypr = matrix2rot(rotmat)
error( nargchk( 1, 1, nargin, 'struct' ) );
error( nargoutchk( 0, 1, nargout, 'struct' ) );

r11 = rotmat(1,1);
r12 = rotmat(1,2);
r13 = rotmat(1,3);

r21 = rotmat(2,1);
r22 = rotmat(2,2);
r23 = rotmat(2,3);

r31 = rotmat(3,1);
r32 = rotmat(3,2);
r33 = rotmat(3,3);


roll = atan2(r21, r11);
pitch = atan2(-r31, sqrt(r32^2+r33^2));
yaw = atan2(r32, r33);

ypr = [roll, pitch, yaw];