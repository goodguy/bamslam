
clear all;

try_index = 2;

odom_real = zeros(4,4,1);
odom_bamslam = zeros(4,4,1);
odom_orbslam = zeros(4,4,1);


count = 0;
max = 1000;


odom_real_origin = eye(4,4);

image = cell(0);
depth = cell(0);

idx_real = 0;
for idx = 1:max
    if exist(sprintf('/home/goodguy/database/navigation/dynamic_fixed_16_11_14_1/odom_%d.txt', idx), 'file') ...
        & exist(sprintf('/home/goodguy/result_bamslam_fixed/odom_%d.txt', idx), 'file') ...
        & exist(sprintf('/home/goodguy/result_orbslam_fixed/odom_%d.txt', idx), 'file')
    
       count = count+1;
       odom_real_curr = load(sprintf('/home/goodguy/database/navigation/dynamic_fixed_16_11_14_1/odom_%d.txt', idx));
       
       odom_real(:,:,count) = odom_real_curr;
       odom_bamslam(:,:,count) = load(sprintf('/home/goodguy/result_bamslam_fixed/odom_%d.txt', idx));
       odom_orbslam(:,:,count) = load(sprintf('/home/goodguy/result_orbslam_fixed/odom_%d.txt', idx));
      
       if(count ~= 1)
           odom_real(:,:,count) = odom_real(:,:,count) * (odom_real_origin^-1);
       else
           odom_real_origin = odom_real(:,:,1);
           odom_real(:,:,1) = eye(4,4);
       end
       idx_real(count) = idx; 
           end
end

step = 1;

compute_error;

%odom_trans_real(:, 2) = -odom_trans_real(:, 2);
figure(1);

interval = 1;
odom_trans = odom_trans_bamslam;
odom_trans_len = length(odom_trans);
plot3(odom_trans(1:interval:odom_trans_len, 1), odom_trans(1:interval:odom_trans_len, 2), odom_trans(1:interval:odom_trans_len, 3), '-.b','linewidth',2);
hold on;
odom_trans = odom_trans_orbslam;
odom_trans_len = length(odom_trans);
plot3(odom_trans(1:interval:odom_trans_len, 1), odom_trans(1:interval:odom_trans_len, 2), odom_trans(1:interval:odom_trans_len, 3), '--m','linewidth',2);
hold off;
axis equal;
axis([-1 1 -1 1 -1 1]);

h_legend = legend('BaM-SLAM', 'ORB-SLAM','Location','northeast');
set(h_legend,'FontSize',12);
set(h_legend,'Interpreter', 'latex');

xlabel('x (m)', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('y (m)', 'Interpreter', 'latex', 'FontSize', 15)
zlabel('z (m)', 'Interpreter', 'latex', 'FontSize', 15)

set(gca, 'XTick', [-3:1:5]);
set(gca, 'YTick',[-4:1:2]);
set(gca, 'ZTick', [-1:0.5:3]);
set(gca, 'FontSize', 12);
set(gca, 'TickLabelInterpreter', 'latex');
set(gca, 'Box', 'off');




