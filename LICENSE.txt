BaM-SLAM is released under a GPLv3 license (see License-gpl.txt).
Please see Dependencies.md for a list of all included code and library dependencies which are not property of the authors of BaM-SLAM.

For a closed-source version of ORB-SLAM2 for commercial purposes, please contact the authors.

If you use BaM-SLAM in an academic work, please cite the most relevant publication associated by visiting:
https://bitbucket.org/goodguy/bamslam




