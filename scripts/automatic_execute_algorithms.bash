#!/bin/bash

scale=0.5
database=~/database/navigation/tum/
for tum_dir in ${database}/* ;
do
    echo "==========================================="
    echo "Start Dataset [$(basename $tum_dir)] "
    if [[ $(basename $tum_dir) == *"freiburg1"* ]]
    then
        benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM1_half.yaml
        #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM1.yaml
        echo "--FREIBURG TYPE 1 DATASET!"
    elif [[ $(basename $tum_dir) == *"freiburg2"* ]]
    then
        #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM2.yaml
        benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM2_half.yaml
        echo "--FREIBURG TYPE 2 DATASET!"
    elif [[ $(basename $tum_dir) == *"freiburg3"* ]]
    then
        #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM3.yaml
        benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/TUM3_half.yaml
        echo "--FREIBURG TYPE 3 DATASET!"
    else
        echo "--FREIBURG TYPE IS UNKNOWN"
        #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/OPENNI2.yaml
        benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/OPENNI2_half.yaml
    fi
    #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/OPENNI2.yaml
    #benchmark_file=~/research_ws/src/bamslam/Examples/RGB-D/OPENNI2_half.yaml

    echo "  -----------------------------------------"
    echo "  Start BAMSLAM"
    echo "  Check Doubleness"
    last_seq="EMPTY"
    is_already_done=false
    mkdir -p `pwd`/experiments/bamslam/
    for dataset_dir in `pwd`/experiments/bamslam/* ;
    do
        echo "$(basename $dataset_dir)"
        if [[ $(basename $dataset_dir) == $(basename $tum_dir) ]]
        then
            echo "$(basename $dataset_dir)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            is_already_done=true
        fi
        last_seq=$(basename $dataset_dir)
    done


    if [[ $is_already_done == false ]]
    then
        rm ~/.ros/KeyFrameTrajectory.txt
        save_loc=`pwd`/experiments/bamslam/$(basename $tum_dir)
        mkdir -p ${save_loc}
        sleep 2
        roslaunch rgbd_saver reader.launch dataset:=$tum_dir camera:=camera color_png:=false > ${save_loc}/saver.log&
        reader_pid=$!
        sleep 2
        roslaunch bamvo bamvo.launch camera:=camera > ${save_loc}/bamvo.log& 
        bamvo_pid=$!
        sleep 2
        roslaunch bamslam bamslam.launch conf_file:=${benchmark_file} scale:=${scale} > ${save_loc}/bamslam.log& 
        algorithm_pid=$!
        sleep 20  
        rostopic pub /camera/next_frame std_msgs/Empty "{}" --once > ${save_loc}/topic.log
        echo "  Wait [$reader_pid]"
        wait $reader_pid
        kill $algorithm_pid
        kill $bamvo_pid
        sleep 10  
        mv ~/.ros/KeyFrameTrajectory.txt ${save_loc}/KeyFrameTrajectory_BAMSLAM.txt 
    else
        echo "  SKIP!!!"
    fi


    echo "  -----------------------------------------"
    echo "  Start ORBSLAM"
    echo "  Check Doubleness"
    last_seq="EMPTY"
    is_already_done=false
    mkdir -p `pwd`/experiments/orbslam/
    for dataset_dir in `pwd`/experiments/orbslam/* ;
    do
        echo "$(basename $dataset_dir)"
        if [[ $(basename $dataset_dir) == $(basename $tum_dir) ]]
        then
            echo "$(basename $dataset_dir)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            is_already_done=true
        fi
        last_seq=$(basename $dataset_dir)
    done


    if [[ $is_already_done == false ]]
    then
        rm ~/.ros/KeyFrameTrajectory.txt
        save_loc=`pwd`/experiments/orbslam/$(basename $tum_dir)
        mkdir -p ${save_loc}
        sleep 2
        roslaunch rgbd_saver reader.launch dataset:=$tum_dir camera:=camera color_png:=false > ${save_loc}/saver.log&
        reader_pid=$!
        sleep 2
        roslaunch bamslam orbslam.launch conf_file:=${benchmark_file} scale:=${scale} > ${save_loc}/bamslam.log& 
        algorithm_pid=$!
        sleep 20  
        rostopic pub /camera/next_frame std_msgs/Empty "{}" --once > ${save_loc}/topic.log
        echo "  Wait [$reader_pid]"
        wait $reader_pid
        kill $algorithm_pid
        sleep 10  
        mv ~/.ros/KeyFrameTrajectory.txt ${save_loc}/KeyFrameTrajectory_ORBSLAM.txt 
    else
        echo "  SKIP!!!"
    fi

    echo "  -----------------------------------------"
    echo "  Start BAMSLAM_DCS"
    echo "  Check Doubleness"
    last_seq="EMPTY"
    is_already_done=false
    mkdir -p `pwd`/experiments/bamslam_dcs/
    for dataset_dir in `pwd`/experiments/bamslam_dcs/* ;
    do
        echo "$(basename $dataset_dir)"
        if [[ $(basename $dataset_dir) == $(basename $tum_dir) ]]
        then
            echo "$(basename $dataset_dir)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            is_already_done=true
        fi
        last_seq=$(basename $dataset_dir)
    done


    if [[ $is_already_done == false ]]
    then
        rm ~/.ros/KeyFrameTrajectory.txt
        save_loc=`pwd`/experiments/bamslam_dcs/$(basename $tum_dir)
        mkdir -p ${save_loc}
        sleep 2
        roslaunch rgbd_saver reader.launch dataset:=$tum_dir camera:=camera color_png:=false > ${save_loc}/saver.log&
        reader_pid=$!
        sleep 2
        roslaunch bamvo bamvo.launch camera:=camera > ${save_loc}/bamvo.log& 
        bamvo_pid=$!
        sleep 2
        roslaunch bamslam bamslam_dcs.launch conf_file:=${benchmark_file} scale:=${scale} > ${save_loc}/bamslam.log& 
        algorithm_pid=$!
        sleep 20  
        rostopic pub /camera/next_frame std_msgs/Empty "{}" --once > ${save_loc}/topic.log
        echo "  Wait [$reader_pid]"
        wait $reader_pid
        kill $algorithm_pid
        kill $bamvo_pid
        sleep 10  
        mv ~/.ros/KeyFrameTrajectory.txt ${save_loc}/KeyFrameTrajectory_BAMSLAM_DCS.txt 
    else
        echo "  SKIP!!!"
    fi


    echo "  -----------------------------------------"
    echo "  Start ORBSLAM_DCS"
    echo "  Check Doubleness"
    last_seq="EMPTY"
    is_already_done=false
    mkdir -p `pwd`/experiments/orbslam_dcs/
    for dataset_dir in `pwd`/experiments/orbslam_dcs/* ;
    do
        echo "$(basename $dataset_dir)"
        if [[ $(basename $dataset_dir) == $(basename $tum_dir) ]]
        then
            echo "$(basename $dataset_dir)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            is_already_done=true
        fi
        last_seq=$(basename $dataset_dir)
    done


    if [[ $is_already_done == false ]]
    then
        rm ~/.ros/KeyFrameTrajectory.txt
        save_loc=`pwd`/experiments/orbslam_dcs/$(basename $tum_dir)
        mkdir -p ${save_loc}
        sleep 2
        roslaunch rgbd_saver reader.launch dataset:=$tum_dir camera:=camera color_png:=false > ${save_loc}/saver.log&
        reader_pid=$!
        sleep 2
        roslaunch bamslam orbslam.launch conf_file:=${benchmark_file} scale:=${scale} > ${save_loc}/bamslam.log& 
        algorithm_pid=$!
        sleep 20  
        rostopic pub /camera/next_frame std_msgs/Empty "{}" --once > ${save_loc}/topic.log
        echo "  Wait [$reader_pid]"
        wait $reader_pid
        kill $algorithm_pid
        sleep 10  
        mv ~/.ros/KeyFrameTrajectory.txt ${save_loc}/KeyFrameTrajectory_ORBSLAM_DCS.txt 
    else
        echo "  SKIP!!!"
    fi
    echo "Finish Dataset [$(basename $tum_dir)]"
done
